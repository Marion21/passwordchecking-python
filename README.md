# PasswordChecking - Python

Creation of a program able to check whether a password is secure.
Constraints :
- min & max length
- must include at least 3 specific characters (digits and punctuation)
- must not be a common password (use of an external file to compare)
- must include both lowercase and uppercase characters
