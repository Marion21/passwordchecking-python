dictChecks = {"longueurMin": 8,
              "longueurMax": 200,
              "countSpecChar": 3,
              "specChar": "àÀâÂäÄåÅçÇéÉèÈêÊëËîÎïÏôÔöÖùÙûÛüÜÿŸ0123456789[@_!#$%^&*()<>?/\|}{~:];"
              }


def check_password():
    password = str(input('Veuillez saisir un mot de passe : '))

    secured = False

    #  length check
    if dictChecks["longueurMin"] <= len(password) <= dictChecks["longueurMax"]:
        secured = True

        # 3 specific characters
        spec_chars = dictChecks["specChar"]
        count = 0
        for char in spec_chars:
            if char in password:
                count += 1
        if count >= dictChecks["countSpecChar"]:
            secured = True

            # Lowercase and Uppercase chars verification
            mixed_case = not password.islower() and not password.isupper()
            if mixed_case:
                secured = True

                # Comparison with most common passwords
                file = open("password.txt", "r", encoding="ISO-8859-1")

                for compromisedPassword in file:
                    if not password == compromisedPassword:
                        secured = True
                file.close()

    if secured == False:
        print("Votre mot de passe n'est pas sécurisé")
        return check_password()
    else:
        print("Votre mot de passe est sécurisé")


check_password()
